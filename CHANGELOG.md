# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2023-05-22

### Added

Custom Exception

- BusinessOmieException

Financial functions:

- searchReleases 
- launchReceipt 
- listAccountsReceivable

General functions:

- getClient 
- listClient 
- setClient 
- connectIdClient 
- deleteClient 
- addClient 
- includeCharacteristicsClient

Service functions:

- addOs 
- getOs 
- setStageOs 
- deleteOs 
- invoiceOs 
- addContract 
- getContract 
- listContract 
- setContract 
- deleteItemContract 
- invoiceContract 
- validateContract