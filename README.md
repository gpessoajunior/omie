# Package Utils

Projeto destinado a fornecer um pacote para integração com o Omie. 

### Pré Requisitos

- PHP 8.1

### Instalação

1. Entre no seu projeto
2. Abra o composer.json
3. Coloque a referência do package dentro da tag repositories (caso não exista crie como no exemplo)
   ```json
   "repositories": [
         {
            "type": "vcs",
            "url": "https://gitlab.com/gpessoajunior/omie"
         }
   ]
   ```
4. Caso tenha um servidor satis referencie como no exemplo a seguir:
   ```json
   "repositories": [
         {
            "type": "composer",
            "url": "https://gitlab.com/gpessoajunior/omie.git"
         }
   ]
   ```
4. Aplique as novas configurações usando o comando composer required
```bash
$ composer required
```
### Features
*optional

Financial API:

| function | doc |

| searchReleases | https://app.omie.com.br/api/v1/financas/pesquisartitulos/#PesquisarLancamentos |

| launchReceipt | https://app.omie.com.br/api/v1/financas/contareceber/#LancarRecebimento/ |

| listAccountsReceivable | https://app.omie.com.br/api/v1/financas/contareceber/#ListarContasReceber |

General API:

| function | doc |

| getClient | https://app.omie.com.br/api/v1/geral/clientes/#ConsultarCliente/ |

| listClient | https://app.omie.com.br/api/v1/geral/clientes/#ListarClientes/ |

| setClient | https://app.omie.com.br/api/v1/geral/clientes/#AlterarCliente/ |

| connectIdClient | https://app.omie.com.br/api/v1/geral/clientes/#AssociarCodIntCliente/ |

| deleteClient | https://app.omie.com.br/api/v1/geral/clientes/#ExcluirCliente/ |

| addClient | https://app.omie.com.br/api/v1/geral/clientes/#IncluirCliente/ |

| includeCharacteristicsClient | https://app.omie.com.br/api/v1/geral/clientescaract/#IncluirCaractCliente |

Service API:

|function | doc |

| addOs | https://app.omie.com.br/api/v1/servicos/os/#IncluirOS |

| getOs | https://app.omie.com.br/api/v1/servicos/os/#ConsultarOS |

| setStageOs | https://app.omie.com.br/api/v1/servicos/os/#TrocarEtapaOS |

| deleteOs | https://app.omie.com.br/api/v1/servicos/os/#ExcluirOS |

| invoiceOs | https://app.omie.com.br/api/v1/servicos/osp/#FaturarOS |

| addContract | https://app.omie.com.br/api/v1/servicos/contrato/#IncluirContrato |

| getContract | https://app.omie.com.br/api/v1/servicos/contrato/#ConsultarContrato |

| listContract | https://app.omie.com.br/api/v1/servicos/contrato/#ListarContratos |

| setContract | https://app.omie.com.br/api/v1/servicos/contrato/#AlterarContrato |

| deleteItemContract | https://app.omie.com.br/api/v1/servicos/contrato/#ExcluirItem |

| invoiceContract | https://app.omie.com.br/api/v1/servicos/contratofat/#FaturarContrato |

| validateContract | https://app.omie.com.br/api/v1/servicos/contratofat/#ValidarContrato |


# Utilização

Para utilizar as funções, instancie a classe omie no projeto.

```php
$omie = new Omie();
```

Em seguida chame a função necessária:

```php
$omie->getClient(['codigo_cliente_omie' => 3303632614]);
```

Para verificação dos erros/exceptions utilize no catch a Exception
```php
catch(BusinessOmieException $e) {

}
```

# License
MIT