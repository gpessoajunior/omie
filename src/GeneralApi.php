<?php

namespace Solides\Business\Omie;

use Solides\Business\Omie\OmieApiAbstract;

class GeneralApi extends OmieApiAbstract
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param string $path
     * @return void
     */
    public function setUrl(string $path): void
    {
        $this->url = $this->getAppUrl() . $path;
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function getClient(array $parameters): ?array
    {
        $this->setUrl('geral/clientes/');
        return $this->request('POST', $this->url, $this->handleBody('ConsultarCliente', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function listClient(array $parameters): ?array
    {
        $this->setUrl('geral/clientes/');
        return $this->request('POST', $this->url, $this->handleBody('ListarClientes', $parameters));
    }

    /**
     * @param array $parameterss
     * @return array|null
     */
    public function setClient(array $parameters): ?array
    {
        $this->setUrl('geral/clientes/');
        return $this->request('POST', $this->url, $this->handleBody('AlterarCliente', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function connectIdClient(array $parameters): ?array
    {
        $this->setUrl('geral/clientes/');
        return $this->request('POST', $this->url, $this->handleBody('AssociarCodIntCliente', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function deleteClient(array $parameters): ?array
    {
        $this->setUrl('geral/clientes/');
        return $this->request('POST', $this->url, $this->handleBody('ExcluirCliente', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function addClient(array $parameters): ?array
    {
        $this->setUrl('geral/clientes/');
        return $this->request('POST', $this->url, $this->handleBody('IncluirCliente', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function includeCharacteristicsClient(array $parameters): ?array
    {
        $this->setUrl('geral/clientescaract/');
        return $this->request('POST', $this->url, $this->handleBody('IncluirCaractCliente', $parameters));
    }
}
