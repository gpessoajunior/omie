<?php

namespace Solides\Business\Omie;

use Solides\Business\Omie\OmieApiAbstract;

class ServiceApi extends OmieApiAbstract
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param string $path
     * @return void
     */
    public function setUrl(string $path): void
    {
        $this->url = $this->getAppUrl() . $path;
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function addOs(array $parameters): ?array
    {
        $this->setUrl('servicos/os/');
        return $this->request('POST', $this->url, $this->handleBody('IncluirOS', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function getOs(array $parameters): ?array
    {
        $this->setUrl('servicos/os/');
        return $this->request('POST', $this->url, $this->handleBody('ConsultarOS', $parameters));
    }

    /**
     * @param array $parameterss
     * @return array|null
     */
    public function setStageOs(array $parameters): ?array
    {
        $this->setUrl('servicos/os/');
        return $this->request('POST', $this->url, $this->handleBody('TrocarEtapaOS', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function deleteOs(array $parameters): ?array
    {
        $this->setUrl('servicos/os/');
        return $this->request('POST', $this->url, $this->handleBody('ExcluirOS', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function invoiceOs(array $parameters): ?array
    {
        $this->setUrl('servicos/osp/');
        return $this->request('POST', $this->url, $this->handleBody('FaturarOS', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function addContract(array $parameters): ?array
    {
        $this->setUrl('servicos/contrato/');
        return $this->request('POST', $this->url, $this->handleBody('IncluirContrato', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function getContract(array $parameters): ?array
    {
        $this->setUrl('servicos/contrato/');
        return $this->request('POST', $this->url, $this->handleBody('ConsultarContrato', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function listContract(array $parameters): ?array
    {
        $this->setUrl('servicos/contrato/');
        return $this->request('POST', $this->url, $this->handleBody('ListarContratos', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function setContract(array $parameters): ?array
    {
        $this->setUrl('servicos/contrato/');
        return $this->request('POST', $this->url, $this->handleBody('AlterarContrato', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function deleteItemContract(array $parameters): ?array
    {
        $this->setUrl('servicos/contrato/');
        return $this->request('POST', $this->url, $this->handleBody('ExcluirItem', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function invoiceContract(array $parameters): ?array
    {
        $this->setUrl('servicos/contratofat/');
        return $this->request('POST', $this->url, $this->handleBody('FaturarContrato', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function validateContract(array $parameters): ?array
    {
        $this->setUrl('servicos/contratofat/');
        return $this->request('POST', $this->url, $this->handleBody('ValidarContrato', $parameters));
    }
}
