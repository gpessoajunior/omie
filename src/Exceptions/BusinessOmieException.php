<?php

namespace Solides\Business\Omie\Exceptions;

use Exception;

class BusinessOmieException extends Exception
{
    /**
     * @param string $errorMessage
     */
    public function __construct(string $errorMessage)
    {
        parent::__construct($errorMessage);
    }
}
