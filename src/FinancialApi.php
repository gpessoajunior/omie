<?php

namespace Solides\Business\Omie;

use Solides\Business\Omie\OmieApiAbstract;

class FinancialApi extends OmieApiAbstract
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param string $path
     * @return void
     */
    public function setUrl(string $path): void
    {
        $this->url = $this->getAppUrl() . $path;
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function searchReleases(array $parameters): ?array
    {
        $this->setUrl('financas/pesquisartitulos/');
        return $this->request('POST', $this->url, $this->handleBody('PesquisarLancamentos', $parameters));
    }

    /**
     * @param array $parameters
     * @return array|null
     */
    public function launchReceipt(array $parameters): ?array
    {
        $this->setUrl('financas/contareceber/');
        return $this->request('POST', $this->url, $this->handleBody('LancarRecebimento', $parameters));
    }

    /**
     * @param array $parameterss
     * @return array|null
     */
    public function listAccountsReceivable(array $parameters): ?array
    {
        $this->setUrl('financas/contareceber/');
        return $this->request('POST', $this->url, $this->handleBody('ListarContasReceber', $parameters));
    }
}
