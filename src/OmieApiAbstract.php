<?php

namespace Solides\Business\Omie;

use Dotenv\Dotenv;
use GuzzleHttp\Client;
use Solides\Business\Omie\Exceptions\BusinessOmieException;

abstract class OmieApiAbstract
{
    private array $header;
    protected $appKey;
    protected $appSecret;
    protected $appUrl;
    protected string $url;
    private $client;

    /**
     * @param string|null $appKey
     * @param string|null $appSecret
     */
    public function __construct()
    {
        $path = getcwd();
        $this->fileExists($path);
        Dotenv::createUnsafeImmutable($path)->load();
        $this->appKey = getenv('OMIE_API_KEY') ?: '';
        $this->appSecret = getenv('OMIE_API_SECRET') ?: '';
        $this->appUrl = getenv('OMIE_API_URL') ?: '';
        $this->validateFields($this->appKey, $this->appSecret, $this->appUrl);
        $this->header = [
            'Content-Type' => 'application/json'
        ];
        $this->client = new Client();
    }

    public function fileExists($path): bool
    {
        if(!file_exists($path. '/.env'))
        {
            throw new BusinessOmieException('Arquivo .env não existe');
        }
        return true;
    }

    public function validateFields($appKey, $appSecret, $appUrl) : bool
    {
        if(empty($appKey) || empty($appSecret) || empty($appUrl))
            throw new BusinessOmieException('Parâmetros para conexão ao omie inválidos');

        $testUrl = get_headers($appUrl)[0];

        if(empty($testUrl) || strpos($testUrl, '404'))
            throw new BusinessOmieException('URL para conexão ao omie inválida');
        return true;
    }

    /**
     * @return string
     */
    public function getAppUrl(): string
    {
        return $this->appUrl;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $auth
     * @return array
     */
    public function request(string $method, string $url, array $body): ?array
    {
        $response = $this->client->request($method, $url, [
                'headers' => $this->header,
                'body' => json_encode($body)
            ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param string $call
     * @param array $parameters
     * @return array
     */
    public function handleBody(string $call, array $parameters): array
    {
        return [
            'call' => $call,
            'app_key' => $this->appKey,
            'app_secret' => $this->appSecret,
            'param' => [
                $parameters
            ]
        ];
    }

    /**
     * @param string $path
     * @return void
     */
    abstract public function setUrl(string $path): void;
}
