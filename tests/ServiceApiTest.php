<?php

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use Solides\Business\Omie\Tests\MockOmieApi;

class ServiceApiTest extends TestCase
{
    #[DataProvider('serviceApiProvider')]
    public function testAddOs($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('serviceApiProvider')]
    public function testGetOs($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('serviceApiProvider')]
    public function testSetStageOst($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('serviceApiProvider')]
    public function testDeleteOs($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('serviceApiProvider')]
    public function testInvoiceOs($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('serviceApiProvider')]
    public function testAddContract($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('serviceApiProvider')]
    public function testGetContract($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('serviceApiProvider')]
    public function testListContract($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('serviceApiProvider')]
    public function testSetContract($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('serviceApiProvider')]
    public function testDeleteItemContract($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('serviceApiProvider')]
    public function testInvoiceContract($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('serviceApiProvider')]
    public function testValidateContract($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    /**
     * @return array
     */
    public static function serviceApiProvider(): array
    {
        $client = new Client(['handler' => MockOmieApi::mockClient()]);

        return [
            'case_200_sucess' => [
                'value' => $client,
                'expected' => 'Success'
            ],
            'case_500_error' => [
                'value' => $client,
                'expected' => 'Server error: `GET /` resulted in a `500 Internal Server Error` response'
            ],
            'case_exception' => [
                'value' => $client,
                'expected' => 'Error Communicating with Server'
            ]
        ];
    }
}

