<?php

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use Solides\Business\Omie\Tests\MockOmieApi;

class GeneralApiTest extends TestCase
{
    #[DataProvider('generalApiProvider')]
    public function testGetClient($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('generalApiProvider')]
    public function testListClient($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('generalApiProvider')]
    public function testSetClient($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('generalApiProvider')]
    public function testConnectIdClient($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('generalApiProvider')]
    public function testDeleteClient($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('generalApiProvider')]
    public function testAddClient($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('generalApiProvider')]
    public function testIncludeCharacteristicsClient($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    /**
     * @return array
     */
    public static function generalApiProvider(): array
    {
        $client = new Client(['handler' => MockOmieApi::mockClient()]);

        return [
            'case_200_sucess' => [
                'value' => $client,
                'expected' => 'Success'
            ],
            'case_500_error' => [
                'value' => $client,
                'expected' => 'Server error: `GET /` resulted in a `500 Internal Server Error` response'
            ],
            'case_exception' => [
                'value' => $client,
                'expected' => 'Error Communicating with Server'
            ]
        ];    
    }
}

