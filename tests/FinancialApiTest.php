<?php

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use Solides\Business\Omie\Tests\MockOmieApi;

class FinancialApiTest extends TestCase
{
    #[DataProvider('financialApiProvider')]
    public function testSearchReleases($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('financialApiProvider')]
    public function testLaunchReceipt($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('financialApiProvider')]
    public function testListAccountsReceivable($value, $expected): void
    {
        try{
            $actual = $value->request('GET', '/')->getBody()->getContents();
            $this->assertEquals($expected, $actual);
        } catch(Exception $e){
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    /**
     * @return array
     */
    public static function financialApiProvider(): array
    {
        $client = new Client(['handler' => MockOmieApi::mockClient()]);

        return [
            'case_200_sucess' => [
                'value' => $client,
                'expected' => 'Success'
            ],
            'case_500_error' => [
                'value' => $client,
                'expected' => 'Server error: `GET /` resulted in a `500 Internal Server Error` response'
            ],
            'case_exception' => [
                'value' => $client,
                'expected' => 'Error Communicating with Server'
            ]
        ];
    }
}

