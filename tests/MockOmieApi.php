<?php

namespace Solides\Business\Omie\Tests;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class MockOmieApi
{ 
    public static function mockClient(): HandlerStack
    {
        $mock = new MockHandler([
            new Response(200, [], 'Success'),
            new Response(500),
            new RequestException('Error Communicating with Server', new Request('POST', '/'))
        ]);

        return HandlerStack::create($mock);
    }
}