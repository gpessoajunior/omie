<?php

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use Solides\Business\Omie\ServiceApi;

class FileApiTest extends TestCase
{
    #[DataProvider('fileExistsProvider')]
    public function testFileExists($value, $expected): void
    {
        $serviceApi = new ServiceApi();
        try{
            $actual = $serviceApi->fileExists($value);
            $this->assertTrue($actual);
        }
        catch(Exception $e)
        {
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    #[DataProvider('validateFieldsProvider')]
    public function testValidateFieldsException($value, $expected): void
    {
        $serviceApi = new ServiceApi();
        try{
            $actual = $serviceApi->validateFields($value['appKey'], $value['appSecret'], $value['appUrl']);
            $this->assertTrue($actual);
        }
        catch(Exception $e)
        {
            $this->assertEquals($expected, $e->getMessage());
        }
    }

    /**
     * @return array
     */
    public static function fileExistsProvider(): array
    {
        return [
            'case_true' => [
                'value' => getcwd(),
                'expected' => true
            ],
            'case_file_no_exist' => [
                'value'=> getcwd().'/.env',
                'expected' => 'Arquivo .env não existe'
            ]
        ];
    }

    /**
     * @return array
     */
    public static function validateFieldsProvider(): array
    {
        return [
            'case_true' => [
                'value' => [
                    'appKey' => '1234567890',
                    'appSecret' => '9876543210',
                    'appUrl' => 'https://app.omie.com.br/api/v1/'
                ],
                'expected' => true
            ],
            'case_appKey_null' => [
                'value' => [
                        'appKey' => '',
                        'appSecret' => '9876543210',
                        'appUrl' => 'https://app.omie.com.br/api/v1/'
                ],
                'expected' => 'Parâmetros para conexão ao omie inválidos'
            ],
            'case_appSecret_null' => [
                'value'=> [
                    'appKey' => '1234567890',
                    'appSecret' => '',
                    'appUrl' => 'https://app.omie.com.br/api/v1/'
                ],
                'expected' => 'Parâmetros para conexão ao omie inválidos'
            ],
            'case_appUrl_null' => [
                'value' => [
                    'appKey' => '1234567890',
                    'appSecret' => '9876543210',
                    'appUrl' => ''
                ],
                'expected' => 'Parâmetros para conexão ao omie inválidos'
            ],
            'case_url_invalid' => [
                'value' => [
                    'appKey' => '1234567890',
                    'appSecret' => '9876543210',
                    'appUrl' => 'app.omie.com.br/api/v1/'
                ],
                'expected' => 'URL para conexão ao omie inválida'
            ]
        ];
    }
}

